﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using LibrairieBasicCounter;
using WindowsFormsApp1;

namespace UnitTestBasicCounter
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        
        public void Testcompteurajout()
        {
            compteur calcul = new compteur (12);
            calcul.ajout();
            Assert.AreEqual(13,calcul.nombre);
        }

        public void Testcompteurmoins()
        {
            compteur calcul = new compteur(12);
            calcul.diminution();
            Assert.AreEqual(11, calcul.nombre);
        }

        public void Testcompteurraz()
        {
            compteur calcul = new compteur(12);
            calcul.raz();
            Assert.AreEqual(0, calcul.nombre);
        }
    }
}
