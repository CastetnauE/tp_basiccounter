﻿namespace WindowsFormsApp1
{
    partial class Form1
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonplus = new System.Windows.Forms.Button();
            this.buttonmoins = new System.Windows.Forms.Button();
            this.buttonraz = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // buttonplus
            // 
            this.buttonplus.Font = new System.Drawing.Font("Impact", 10F);
            this.buttonplus.Location = new System.Drawing.Point(514, 123);
            this.buttonplus.Name = "buttonplus";
            this.buttonplus.Size = new System.Drawing.Size(80, 34);
            this.buttonplus.TabIndex = 0;
            this.buttonplus.Text = "+";
            this.buttonplus.UseVisualStyleBackColor = true;
            this.buttonplus.Click += new System.EventHandler(this.buttonplus_Click);
            // 
            // buttonmoins
            // 
            this.buttonmoins.Font = new System.Drawing.Font("Impact", 10F);
            this.buttonmoins.Location = new System.Drawing.Point(240, 123);
            this.buttonmoins.Name = "buttonmoins";
            this.buttonmoins.Size = new System.Drawing.Size(79, 34);
            this.buttonmoins.TabIndex = 1;
            this.buttonmoins.Text = "-";
            this.buttonmoins.UseVisualStyleBackColor = true;
            this.buttonmoins.Click += new System.EventHandler(this.buttonmoins_Click);
            // 
            // buttonraz
            // 
            this.buttonraz.Font = new System.Drawing.Font("Impact", 10F);
            this.buttonraz.Location = new System.Drawing.Point(366, 191);
            this.buttonraz.Name = "buttonraz";
            this.buttonraz.Size = new System.Drawing.Size(90, 33);
            this.buttonraz.TabIndex = 2;
            this.buttonraz.Text = "RAZ";
            this.buttonraz.UseVisualStyleBackColor = true;
            this.buttonraz.Click += new System.EventHandler(this.buttonraz_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Impact", 20F);
            this.label1.Location = new System.Drawing.Point(377, 75);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(70, 34);
            this.label1.TabIndex = 5;
            this.label1.Text = "Total";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Impact", 30F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label2.Location = new System.Drawing.Point(394, 123);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(42, 48);
            this.label2.TabIndex = 6;
            this.label2.Text = "0";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Info;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.buttonraz);
            this.Controls.Add(this.buttonmoins);
            this.Controls.Add(this.buttonplus);
            this.ImeMode = System.Windows.Forms.ImeMode.On;
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonplus;
        private System.Windows.Forms.Button buttonmoins;
        private System.Windows.Forms.Button buttonraz;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
    }
}

