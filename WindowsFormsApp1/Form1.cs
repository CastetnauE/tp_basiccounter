﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LibrairieBasicCounter;

namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {   
        compteur calcul;
        public Form1()
        {
            InitializeComponent();
            this.calcul = new compteur(0);
            
        }
       
        private void buttonplus_Click(object sender, EventArgs e)
        {
           label2.Text = String.Format( calcul.ajout().ToString());
           
        }

        private void buttonmoins_Click(object sender, EventArgs e)
        {
            label2.Text = String.Format(calcul.diminution().ToString());    
        }

        private void buttonraz_Click(object sender, EventArgs e)
        {
            label2.Text =String.Format(calcul.raz().ToString());
        }

       
    }
}
